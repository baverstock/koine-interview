# KoineInterview

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3 and requires nodejs and npm

## Tasks

1. Connect Koine service to the mock Koine API
2. Retrieve a list of Koine accounts
3. Display accounts using the koine-card component
4. Connect Coinbase service to the real Coinbase API
5. Find a graphing library and include in the project
6. For selected Koine account get past 24 hour market data from Coinbase
7. Display market data in your graph
8. Once finsihed pushed the code in your branch and create a merge request in Gitlab

## Example
Below is a quick example on what the app should look like once you have hooked up the Koine API. You still need to add the graph in.

![alt text](https://gitlab.com/koine-solace/koine-interview/-/raw/master/docs/app-helper.PNG)

## Koine API
We have mocked the Koine API on Apiary. You can find the link here: [Koine API](https://app.apiary.io/koineinterview/editor). There is only one endpoint that you will need to query: `/accounts`

## Coinbase API
You can connect to the real Coinbase API for free. You can find a link to the docs here: [Coinbase API](https://docs.pro.coinbase.com/#introduction). You only need to use one endpoint: `/products/{symbol}-USD/candles`

This will give you historic rates for the given symbol i.e. `BTC` you can also pass extra query parameters to get rates for a given time period which you should use to get the past 24 hours.

Find the historic rate docs here: [Historic Rates](https://docs.pro.coinbase.com/#get-historic-rates)

## Development server

Run `npm install` to load the dependancies.  Once complete run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
