import { TestBed } from '@angular/core/testing';

import { KoineService } from './koine.service';

describe('KoineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KoineService = TestBed.get(KoineService);
    expect(service).toBeTruthy();
  });
});
