import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'koine-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {

  @Input() symbol: string;

  constructor() { }

  ngOnInit() {
  }

}
