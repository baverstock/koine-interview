import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// external modules
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// koine services
import { KoineService } from '../services/koine.service';
import { CoinbaseService } from '../services/coinbase.service';

// koine components
import { HeaderComponent } from './header/header.component';
import { SubHeaderComponent } from './sub-header/sub-header.component';
import { AssetCardComponent } from './asset-card/asset-card.component';
import { GraphComponent } from './graph/graph.component';
import { DashboardComponent } from './dashboard/dashboard.component';




@NgModule({
  declarations: [
    HeaderComponent,
    SubHeaderComponent,
    AssetCardComponent,
    GraphComponent,
    DashboardComponent,
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatCardModule,
    FontAwesomeModule
  ],
  exports: [
    DashboardComponent
  ],
  providers: [
    KoineService,
    CoinbaseService
  ]
})
export class DashboardModule { }
